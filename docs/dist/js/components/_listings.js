/*!
 * fly-front v1.0.1: Fly.fr frontend project
 * (c) 2017 Amine Mbarki
 * MIT License
 * https://bitbucket.org/flyfr/fly-front
 */

/**
 * Created by amine on 10/28/2016.
 */



(function($,ui){
	'use strict';
	
	$ ( function () {
		
		
		// bind change event to select
		$ ( " .block-filter input[type='checkbox']" ).on ( 'change' , function () {
			var url = $ ( this ).val (); // get selected value
			if ( url ) { // require a URL
				window.location = url; // redirect
			}
			return false;
		} );
		
		// Reset filter button
		$ ( '#reset-filter' ).click ( function () {
			$ ( ".block-filter input[type=checkbox]" ).prop ( 'checked' , false );
		} );
		
		// display-product-option sidebar mobile
		$ ( ".display-product-option" ).clone ().insertAfter ( ".side-nav-categories" ).addClass ( 'rendered' );
		
		// sidebar mobile nav
		$ ( '.side-nav-categories .block-title' ).click ( function () {
			$ ( '.side-nav-categories .category-menu' ).collapse ( 'toggle' );
		} );
		
		// sidebar filter toggle
		$ ( '.show-filter-mobile' ).click ( function () {
			$ ( '.block-filter' ).collapse ( 'toggle' );
		} );
		
		// Render Product ON hover
		ui.renderProductsListe();


	} );
})(jQuery,ui);